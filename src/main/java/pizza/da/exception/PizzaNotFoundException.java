package pizza.da.exception;

public class PizzaNotFoundException extends RuntimeException {

    public PizzaNotFoundException(int id) {
        super("Ошибка: отсутствует пицца с id=" + id);
    }
}
