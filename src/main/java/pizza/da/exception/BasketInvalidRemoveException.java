package pizza.da.exception;

public class BasketInvalidRemoveException extends RuntimeException {

    public BasketInvalidRemoveException() {
        super("Ошибка. Количество заказанной пиццы не может быть отрицательным.");
    }
}
