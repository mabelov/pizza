package pizza.da.service;

import pizza.da.exception.BasketInvalidRemoveException;

import java.util.Map;

public interface BasketDaService {

    /**
     * Получить корзину
     *
     * @return список пар <пицца - кол-во>
     */
    Map<Integer, Integer> getBasket();

    /**
     * Добавить одну пиццу в корзину
     *
     * @param pizzaId идентификатор добавляемой пиццы
     */
    void addPizza(int pizzaId);

    /**
     * Уменьшить количество пицц в корзине на одну. При обнулении количества пицца убирается из корзины совсем.
     *
     * @param pizzaId идентификатор пиццы
     * @throws BasketInvalidRemoveException указанная пицца отсутствует в корзине, уменьшение количества невозможно
     */
    void removePizza(int pizzaId) throws BasketInvalidRemoveException;
}
