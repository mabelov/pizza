package pizza.da.service;

import pizza.da.exception.PizzaNotFoundException;
import pizza.da.vo.Pizza;

import java.util.List;

public interface PizzaDaService {

    /**
     * @return Список всех пицц интернет-магазина
     */
    List<Pizza> getPizzas();

    /**
     * Поиск пиццы по идентификатору
     * @param pizzaId идентификатор пиццы
     * @return найденная пицца
     * @throws PizzaNotFoundException в случае отсутствия писсы с указанным идентификатором
     */
    default Pizza getPizza(int pizzaId) throws PizzaNotFoundException {
        return getPizzas().stream()
                .filter(pizza -> pizza.getId() == pizzaId)
                .findFirst()
                .orElseThrow(() -> new PizzaNotFoundException(pizzaId));
    }
}
