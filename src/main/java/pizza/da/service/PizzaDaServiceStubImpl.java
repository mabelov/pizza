package pizza.da.service;

import org.springframework.stereotype.Component;
import pizza.da.exception.PizzaNotFoundException;
import pizza.da.vo.Pizza;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@Component
public class PizzaDaServiceStubImpl implements PizzaDaService {

    private static int currentId = 0;

    private static int nextId() {
        return ++currentId;
    }

    private static final List<Pizza> PIZZAS = asList(
            new Pizza(
                    nextId(),
                    "Маргарита",
                    "Томатный соус, сыр, помидоры.",
                    560,
                    820,
                    "margarita.jpg"),
            new Pizza(
                    nextId(),
                    "Дон Бекон",
                    "Томатный соус, сыр, много бекона, помидорки черри.",
                    510,
                    760,
                    "don-bekon.jpg"),
            new Pizza(
                    nextId(),
                    "Праздничная",
                    "Фирменный соус, сыр, ветчина, бекон, охотничьи колбаски, шампиньоны, брокколи",
                    500,
                    800,
                    "prazdnichnaya.jpg"),
            new Pizza(
                    nextId(),
                    "По-домашнему",
                    "Пицца со вкусом домашней щедрости и уюта. Фирменный соус, сыр, ветчина, салями",
                    480,
                    830,
                    "po-domashnemu.jpg"),
            new Pizza(
                    nextId(),
                    "Жюльен",
                    "Соус Цезарь, куриная грудка, сыр моцарелла, сыр пармезан, сливки, шампиньоны",
                    500,
                    760,
                    "zhyulen.jpg"),
            new Pizza(
                    nextId(),
                    "Чесночный цыпа",
                    "Чесночный соус, сыр, куриная грудка, бекон, помидоры.",
                    475,
                    770,
                    "chesnochnyy-cypa.jpg"),
            new Pizza(
                    nextId(),
                    "Мясное плато",
                    "Томатный соус, сыр, бекон, салями, ветчина, колбаса п/к",
                    480,
                    790,
                    "myasnoe-plato.jpg"),
            new Pizza(
                    nextId(),
                    "Баварская",
                    "Томатный соус, сыр, бастурма, колбаски пепперони, колбаски охотничьи, чеснок, перчик",
                    470,
                    800,
                    "bavarskaya.jpg"),
            new Pizza(
                    nextId(),
                    "Студенческая",
                    "Фирменный соус, майонез, сыр, колбаса студенческая, помидоры. В мелкую нарезку",
                    450,
                    790,
                    "studencheskaya.jpg"),
            new Pizza(
                    nextId(),
                    "Пока все дома",
                    "Соус ранч, сыр моцарелла, салями, ветчина, бекон, болгарский перец зеленый.",
                    400,
                    730,
                    "poka-vse-doma.jpg")
    );

    @Override
    public List<Pizza> getPizzas() {
        return new ArrayList<>(PIZZAS);
    }
}
