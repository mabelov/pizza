package pizza.da.service;

import org.springframework.stereotype.Component;
import pizza.da.exception.BasketInvalidRemoveException;

import java.util.HashMap;
import java.util.Map;

@Component
public class BasketDaServiceStubImpl implements BasketDaService {

    private final Map<Integer, Integer> basket = new HashMap<>();

    @Override
    public Map<Integer, Integer> getBasket() {
        return new HashMap<>(basket);
    }

    @Override
    public void addPizza(int pizzaId) {
        Integer count = basket.getOrDefault(pizzaId, 0);
        basket.put(pizzaId, count + 1);
    }

    @Override
    public void removePizza(int pizzaId) throws BasketInvalidRemoveException {
        Integer count = basket.getOrDefault(pizzaId, 0);
        if (count == 0) {
            throw new BasketInvalidRemoveException();
        }
        if (count == 1) {
            basket.remove(pizzaId);
        } else {
            basket.put(pizzaId, count - 1);
        }
    }
}
