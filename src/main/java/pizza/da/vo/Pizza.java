package pizza.da.vo;

import java.util.Objects;

public class Pizza {

    private final int id;
    private final String name;
    private final String description;
    private final int cost;
    private final int weight;
    private final String image;

    public Pizza(int id, String name, String description, int cost, int weight, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.weight = weight;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getCost() {
        return cost;
    }

    public int getWeight() {
        return weight;
    }

    public String getImage() {
        return image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pizza)) {
            return false;
        }
        Pizza pizza = (Pizza) o;
        return cost == pizza.cost &&
                weight == pizza.weight &&
                name.equals(pizza.name) &&
                Objects.equals(description, pizza.description) &&
                Objects.equals(image, pizza.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, cost, weight, image);
    }
}
