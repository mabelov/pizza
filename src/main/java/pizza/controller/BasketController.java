package pizza.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pizza.da.service.BasketDaService;
import pizza.da.service.PizzaDaService;
import pizza.da.vo.Pizza;

import java.util.Map;
import java.util.stream.Collectors;

@Controller
@Component
public class BasketController {

    @Autowired
    private BasketDaService basketDaService;
    @Autowired
    private PizzaDaService pizzaDaService;

    @GetMapping("/refreshBasket")
    public String refreshBasket(Map<String, Object> model) {
        return updateView(model);
    }

    @GetMapping("/addToBasket")
    public String addToBasket(@RequestParam("pizzaId") int pizzaId, Map<String, Object> model) {
        basketDaService.addPizza(pizzaId);
        return updateView(model);
    }

    @GetMapping("/removeFromBasket")
    public String removeFromBasket(@RequestParam("pizzaId") int pizzaId, Map<String, Object> model) {
        basketDaService.removePizza(pizzaId);
        return updateView(model);
    }

    private Map<Pizza, Integer> getBasketView() {
        Map<Integer, Integer> basket = basketDaService.getBasket();
        return basket.entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> {
                            int pizzaId = entry.getKey();
                            return pizzaDaService.getPizza(pizzaId);
                        },
                        Map.Entry::getValue
                ));
    }

    private int getSumCost(Map<Pizza, Integer> basketView) {
        return basketView.entrySet().stream()
                .map(entry -> {
                    int cost = entry.getKey().getCost();
                    int count = entry.getValue();
                    return cost * count;
                })
                .reduce(0, Integer::sum);
    }

    private String updateView(Map<String, Object> model) {
        Map<Pizza, Integer> basketView = getBasketView();
        int sumCost = getSumCost(basketView);
        model.put("basket", basketView);
        model.put("basketSumCost", sumCost);
        return "welcome :: #basket";
    }
}
