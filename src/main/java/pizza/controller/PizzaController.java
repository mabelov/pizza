package pizza.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pizza.da.service.PizzaDaService;
import pizza.da.vo.Pizza;

import java.util.List;
import java.util.Map;

@Controller
public class PizzaController {

    @Autowired
    private PizzaDaService pizzaDaService;
    @Autowired
    private BasketController basketController;

    /**
     * Обработчик запроса главной страницы
     *
     * @param model данные, отображаемые пользователю
     * @return имя html-thymeleaf-шаблона (welcome.html)
     */
    @RequestMapping("/")
    public String pizzas(Map<String, Object> model) {
        List<Pizza> pizzas = pizzaDaService.getPizzas();
        model.put("pizzas", pizzas);
        basketController.refreshBasket(model);
        return "welcome";
    }
}