function addToBasket(pizzaId) {
  $.ajax({
    type: "GET",
    url: '/addToBasket',
    data: {pizzaId: pizzaId},
    success: function(fragment) {
      $("#basket").replaceWith(fragment);
    },
    error: function(xhr) {
      alert("Ошибка добавления товара в корзину: " + xhr.responseJSON.error);
    }
  })
}

function removeFromBasket(pizzaId) {
  $.ajax({
    type: "GET",
    url: '/removeFromBasket',
    data: {pizzaId: pizzaId},
    success: function(fragment) {
      $("#basket").replaceWith(fragment);
    },
    error: function(xhr) {
      alert("Ошибка удаления пиццы из корзины: " + xhr.responseJSON.error);
    }
  })
}

function refreshBasket() {
  $.ajax({
    type: "GET",
    url: '/refreshBasket',
    success: function(fragment) {
      $("#basket").replaceWith(fragment);
    },
    error: function(xhr) {
      alert("Ошибка обновления корзины: " + xhr.responseJSON.error);
    }
  })
}